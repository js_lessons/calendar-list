const content = document.querySelector(".calendar-content");
const settingDate = document.querySelector(".settingDate");

function addDays(numDay, css) {
  const day = document.createElement("span");
  day.classList.add(css);
  day.innerText = numDay;
  content.append(day);
}

function createCalendar(year, month) {
  let date = new Date(year, month - 1);
  settingDate.innerText = date;

  let lastDate = new Date(date.setMonth(date.getMonth() + 1));
  lastDate = new Date(lastDate.setDate(lastDate.getDate() - 1));

  date = new Date(year, month - 1);
  let prevDate = new Date(date.setDate(date.getDate() - 1));

  let prevDays = new Date(year, month - 1).getDay() - 1;
  let nextDays = 7 - lastDate.getDay();

  const arrPrev = getPrevDay(prevDate.getDate(), prevDays);
  for (let i = 0; i < arrPrev.length; i++) {
    addDays(arrPrev[i], "prevDay");
  }

  const arrCurent = getNextDay(lastDate.getDate());
  for (let i = 0; i < arrCurent.length; i++) {
    addDays(arrCurent[i], "currentDay");
  }

  const arrNext = getNextDay(nextDays);
  for (let i = 0; i < arrNext.length; i++) {
    addDays(arrNext[i], "nextDay");
  }
}

function getPrevDay(day, count) {
  let result = [];
  for (let i = count; i > 0; i--) {
    result.unshift(day--);
  }
  return result;
}

function getNextDay(count) {
  let result = [];
  for (let i = 1; i <= count; i++) {
    result.push(i);
  }
  return result;
}

// function getCurrentDay(count) {
//   let result = [];
//   for (let i = 1; i <= count; i++) {
//     result.push(i);
//   }
//   return result;
// }

createCalendar(2022, 10);
